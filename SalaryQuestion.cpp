#include<iostream>//header files
using namespace std;//library inclusion
int main()
{
    int t;
    cin>>t;
    int element;
    while(t--)
    {
        int arr[3];
        for(int i=0;i<3;i++)
        {
            cin>>element;
            arr[i]=element;
        }
        sort(arr,arr+3);//in_built function works in O(nlogn) time Quick Sort Algorithm of c++
        //standard template library 
      
        cout<<arr[1]<<"\n";
    }
    return 0; 
}